FROM openjdk:11-jre
#Открываем порт в docker container
EXPOSE 8029
#Создаем переменную которая хранит путь до jar файла
ARG JAR_FILE=build/libs/initialization-database-1.0.jar
#Копируем jar файл в docker container
ADD ${JAR_FILE} initialization-database.jar
#Запускам jar файл
ENTRYPOINT ["java","-jar","initialization-database.jar"]