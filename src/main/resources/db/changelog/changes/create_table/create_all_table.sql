--liquibase formatted sql

--changeset test:1
create table if not exists department
(
    id                        serial primary key,
    name                      varchar(64)  not null unique,
    password                  varchar(255) not null,
    email                     varchar(255) not null unique,
    role                      varchar(24)  not null,
    time_registration_company date
);

--changeset test:2
create table if not exists driver
(
    id            serial primary key,
    name          varchar(64)  not null,
    department_id int          not null,
    email         varchar(255) not null unique,
    password      varchar(255) not null,
    role          varchar(24)  not null,
    foreign key (department_id) references department (id)
);

--changeset test:3
create table if not exists orders
(
    id                    serial primary key,
    address_to            varchar(255) not null,
    address_from          varchar(255) not null,
    time_completion_order varchar(24),
    time_create_order     varchar(24)  not null,
    description           varchar(255) not null,
    status                varchar(64)  not null,
    phone                 varchar(16)  not null,
    name_client           varchar(64)  not null,
    department_id         int          not null,
    driver_id             int,
    foreign key (department_id) references department (id),
    foreign key (driver_id) references driver (id),
    unique (time_create_order, phone)
);




