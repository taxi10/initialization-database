package com.test.initializationdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InitializationDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(InitializationDatabaseApplication.class, args);
    }

}
